<?php

namespace App\Http\Controllers\Api\Customer;

use App\Helper\Helper;
use App\Helper\StatusDefineCode;
use App\Helper\TypeFCM;
use App\Http\Controllers\Controller;
use App\Jobs\PushNotificationUserJob;
use App\Models\Agency;
use App\Models\AgencyBonusStep;
use App\Models\AgencyImportStep;

use App\Models\AgencyConfig;
use App\Models\AgencyImportConfig;
use App\Models\AgencyRegisterRequest;
use App\Models\AgencysConfig;
use App\Models\BonusAgency;
use App\Models\ChangeBalanceAgency;
use App\Models\Customer;
use App\Models\HistoryBonusAgency;
use App\Models\MsgCode;
use App\Models\Order;
use App\Models\PayAgency;
use App\Models\PayRewardAgency;
use App\Services\BalanceCustomerService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

/**
 * @group  Customer/Đại lý
 */
class CustomerAgencyController extends Controller
{

    /**
     * Thông tin Đại lý
     * @bodyParam is_agency bool đăng ký hay không hay không (true false)
     * @bodyParam  payment_auto boolean Bật tự động để user quyết toán 
     * @bodyParam  first_and_last_name Họ và tên
     * @bodyParam  cmnd CMND
     * @bodyParam  date_range ngày cấp
     * @bodyParam  issued_by Nơi cấp
     * @bodyParam  front_card Mặt trước link
     * @bodyParam  back_card Mật sau link
     * @bodyParam  bank Tên ngân hàng
     * @bodyParam  account_number Số tài khoản
     * @bodyParam  account_name Tên tài khoản
     * @bodyParam  branch Chi nhánh
     */
    public function editProfile(Request $request)
    {
        $data = [
            'store_id' => $request->store->id,
            'customer_id' => $request->customer->id,
            'payment_auto' =>  filter_var($request->payment_auto, FILTER_VALIDATE_BOOLEAN),
            'first_and_last_name' => $request->first_and_last_name,
            'cmnd' => $request->cmnd,
            'date_range' => $request->date_range,
            'issued_by' => $request->issued_by,
            'front_card' => $request->front_card,
            'back_card' => $request->back_card,
            'bank' => $request->bank,
            'account_number' => $request->account_number,
            'account_name' => $request->account_name,
            'branch' => $request->branch,
        ];

        $agency = Agency::where('store_id', $request->store->id)->where('customer_id', $request->customer->id)->first();

        if ($agency == null) {
            Agency::create(
                $data
            );
        } else {
            $agency->update(
                $data
            );
        }


        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
            'data' => Agency::where('store_id', $request->store->id)
                ->where('customer_id', $request->customer->id)->first()
        ], 200);
    }


    public function info_account(Request $request)
    {

        $agency = Agency::where('store_id', $request->store->id)
            ->where('customer_id', $request->customer->id)->first();

        if ($agency == null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_REGISTERED_COLLABORATOR[0],
                'msg' => MsgCode::NOT_REGISTERED_COLLABORATOR[1],
            ], 400);
        }


        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
            'data' => Agency::where('store_id', $request->store->id)->where('customer_id', $request->customer->id)->first()
        ], 200);
    }

    /**
     * Thông tin tổng quan
     * 
     * Doanh thu hiện tại
     * 
     * type_rose 0 doanh sô - 1 hoa hồng
     * 
     * total_final daonh số tháng này
     * 
     * share_agency tổng tiền hoa đồng chia sẻ
     * 
     * received_month_bonus Đã nhận thưởng tháng hay chưa
     * 
     * number_order số lượng đơn hàng tháng này
     * 
     * allow_payment_request cho phép yêu cầu thanh toán
     * 
     * payment_1_of_month định kỳ thanh toán 1
     * 
     * payment_16_of_month định kỳ thanh toán 15
     * 
     * payment_limit Giới hạn yêu cầu thanh toán 
     * 
     * has_payment_request có yêu cầu thanh toán hay không
     * 
     * money_payment_request Số tiền yêu cầu hiện tại
     * 
     */
    public function info_overview(Request $request)
    {

        $carbon = Carbon::now('Asia/Ho_Chi_Minh');

        $dateFromDay = $carbon->year . '-' . $carbon->month . '-' . $carbon->day . ' 00:00:00';
        $dateToDay = $carbon->year . '-' . $carbon->month . '-' . $carbon->day . ' 23:59:59';

        $dateFromMonth = $carbon->year . '-' . $carbon->month . '-' . "01" . ' 00:00:00';
        $dateToMonth = $carbon->year . '-' . $carbon->month . '-' . $carbon->day . ' 23:59:59';

        $dateFromYear = $carbon->year . '-' . "01" . '-' . "01" . ' 00:00:00';
        $dateToYear = $carbon->year . '-' . $carbon->month . '-' . $carbon->day . ' 23:59:59';

        $date = new \Carbon\Carbon();
        $firstOfQuarter = $date->firstOfQuarter();
        $lastOfQuarter = $date->lastOfQuarter();

        $curMonth = date("m", time());
        $curQuarter = (int)ceil($curMonth / 3);

        $dateFromQuarter = $firstOfQuarter->year . '-' .  $curQuarter . '-' . "01" . ' 00:00:00';
        $dateToQuarter =  $carbon->year . '-' . $carbon->month . '-' . $carbon->day . ' 23:59:59';

        $day = date('w');
        $week_start = date('Y-m-d', strtotime('-' . $day . ' days'));
        $week_end = date('Y-m-d', strtotime('+' . (6 - $day) . ' days'));

        $dateFromWeek = $week_start  . ' 00:00:00';
        $dateToWeek = $week_end . ' 23:59:59';


        // $total_order = Order::where('store_id', $request->store->id)
        //     ->where('agency_by_customer_id', $request->customer->id)
        //     ->where('order_status', StatusDefineCode::COMPLETED)
        //     ->where('payment_status', StatusDefineCode::PAID)
        //     ->sum('total_final');

        //  whereIn('orders.phone_number', [0977711111,0955566666,0987654321,0988776611,0977744444])
        //
        $count_in_day = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromDay)
            ->where('orders.created_at', '<=', $dateToDay)
            ->count();

        $total_final_in_day = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromDay)
            ->where('orders.created_at', '<=', $dateToDay)
            ->sum('total_final');
        //

        $count_in_month = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromMonth)
            ->where('orders.created_at', '<=', $dateToMonth)
            ->count();

        $total_final_in_month = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromMonth)
            ->where('orders.created_at', '<=', $dateToMonth)
            ->sum('total_final');
        //
        $count_in_week = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromWeek)
            ->where('orders.created_at', '<=', $dateToWeek)
            ->count();

        $total_final_in_week = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromWeek)
            ->where('orders.created_at', '<=', $dateToWeek)
            ->sum('total_final');
        //
        $count_in_year = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromYear)
            ->where('orders.created_at', '<=', $dateToYear)
            ->count();

        $total_final_in_year = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromYear)
            ->where('orders.created_at', '<=', $dateToYear)
            ->sum('total_final');
        //

        $count_in_quarter = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromQuarter)
            ->where('orders.created_at', '<=', $dateToQuarter)
            ->count();

        $total_final_in_quarter = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromQuarter)
            ->where('orders.created_at', '<=', $dateToQuarter)
            ->sum('total_final');

        //-//////////////

        $total_after_discount_no_bonus_in_day = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromDay)
            ->where('orders.created_at', '<=', $dateToDay)
            ->sum(DB::raw('total_before_discount - combo_discount_amount - product_discount_amount - voucher_discount_amount'));
        //

        $total_after_discount_no_bonus_in_month = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromMonth)
            ->where('orders.created_at', '<=', $dateToMonth)
            ->sum(DB::raw('total_before_discount - combo_discount_amount - product_discount_amount - voucher_discount_amount'));


        $total_after_discount_no_bonus_in_week = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromWeek)
            ->where('orders.created_at', '<=', $dateToWeek)
            ->sum(DB::raw('total_before_discount - combo_discount_amount - product_discount_amount - voucher_discount_amount'));


        $total_after_discount_no_bonus_in_year = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromYear)
            ->where('orders.created_at', '<=', $dateToYear)
            ->sum(DB::raw('total_before_discount - combo_discount_amount - product_discount_amount - voucher_discount_amount'));

        $total_after_discount_no_bonus_in_quarter = Order::where('store_id', $request->store->id)
            ->where('agency_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('orders.created_at', '>=', $dateFromQuarter)
            ->where('orders.created_at', '<=', $dateToQuarter)
            ->sum(DB::raw('total_before_discount - combo_discount_amount - product_discount_amount - voucher_discount_amount'));


        $agency = Agency::where('store_id', $request->store->id)
            ->where('customer_id', $request->customer->id)->first();

        if ($agency == null) {
            $agency = Agency::create(
                [
                    'store_id' => $request->store->id,
                    'customer_id' =>  $request->customer->id
                ]
            );
        }

        $configExists = AgencyConfig::where(
            'store_id',
            $request->store->id
        )->first();

        if ($configExists == null) {
            return response()->json([
                'code' => 200,
                'success' => true,
                'msg_code' => MsgCode::SUCCESS[0],
                'msg' => MsgCode::SUCCESS[1],
                'data' => [
                    'count_in_day' =>  $count_in_day,
                    'total_final_in_day' =>  $total_final_in_day,

                    'count_in_month' =>  $count_in_month,
                    'total_final_in_month' =>  $total_final_in_month,

                    'count_in_week' =>  $count_in_week,
                    'total_final_in_week' =>  $total_final_in_week,

                    'count_in_year' =>  $count_in_year,
                    'total_final_in_year' =>  $total_final_in_year,

                    'count_in_quarter' =>  $count_in_quarter,
                    'total_final_in_quarter' =>  $total_final_in_quarter,

                    //

                    'total_after_discount_no_bonus_in_day' =>  $total_after_discount_no_bonus_in_day,


                    'total_after_discount_no_bonus_in_month' =>  $total_after_discount_no_bonus_in_month,


                    'total_after_discount_no_bonus_in_week' =>  $total_after_discount_no_bonus_in_week,


                    'total_after_discount_no_bonus_in_year' =>  $total_after_discount_no_bonus_in_year,


                    'total_after_discount_no_bonus_in_quarter' =>  $total_after_discount_no_bonus_in_quarter,

                    "balance" =>  0,
                    "total_final" => 0,
                    "number_order" => 0,
                    "share_agency_ctv" =>  0,
                    "total_final_ctv" =>  0,
                    "number_order_ctv" => 0,
                    "received_month_bonus" => 0,
                    "type_rose" => 0,
                    "allow_payment_request" => false,
                    "payment_1_of_month" => 0,
                    "payment_16_of_month" => 0,
                    "payment_limit" =>  0,
                    "has_payment_request" => false,
                    "money_payment_request" => 0,
                    "steps_bonus" => array(),
                ]
            ], 200);
        }


        $payAfter = PayAgency::where('store_id', $request->store->id)
            ->where('agency_id',  $agency->id)->where('status', 0)->first();

        //Thưởng hoa hồng
        $steps = AgencyBonusStep::where('store_id', $request->store->id)->orderBy('bonus', 'asc')->get();
        // Thưởng doanh số
        $steps_import = AgencyImportStep::where('store_id', $request->store->id)->orderBy('bonus', 'asc')->get();


        $now = Helper::getTimeNowDateTime();


        //Phần nhập hàng
        $total_final = Order::where('store_id', $request->store->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('agency_by_customer_id', $request->customer->id)
            ->sum('total_final');

        $total_after_discount_no_bonus = Order::where('store_id', $request->store->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('agency_by_customer_id', $request->customer->id)
            ->sum(DB::raw('total_before_discount - combo_discount_amount - product_discount_amount - voucher_discount_amount'));


        $count = Order::where('store_id', $request->store->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('agency_by_customer_id', $request->customer->id)->count();

        $history = HistoryBonusAgency::where('store_id', $request->store->id)
            ->where('agency_id', $agency->id)
            ->where('year', $request->year)
            ->where('month', $request->month)->first();

        $total_final_ctv = Order::where('store_id', $request->store->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('agency_ctv_by_customer_id', $request->customer->id)
            ->orWhere('agency_ctv_by_customer_referral_id', $request->customer->id)
            ->whereBetween('created_at', [$now->format('Y-m-01 00:00:00'), $now->format('Y-m-d H:i:s')])
            ->sum('total_final');

        $share_agency_ctv = Order::where('store_id', $request->store->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('agency_ctv_by_customer_id', $request->customer->id)
            ->whereBetween('created_at', [$now->format('Y-m-01 00:00:00'), $now->format('Y-m-d H:i:s')])
            ->sum('share_agency');

        $share_agency_ctv =  $share_agency_ctv + Order::where('store_id', $request->store->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where('agency_ctv_by_customer_referral_id', $request->customer->id)
            ->whereBetween('created_at', [$now->format('Y-m-01 00:00:00'), $now->format('Y-m-d H:i:s')])
            ->sum('share_agency_referen');

        $count_ctv = Order::where('store_id', $request->store->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->where(function ($query) use ($request) {
                $query->where('agency_ctv_by_customer_id', $request->customer->id)
                    ->orWhere('agency_ctv_by_customer_referral_id', $request->customer->id);
            })
            ->where(function ($query) use ($request) {
                $query->where('orders.share_agency', '>', 0)
                    ->orWhere('orders.share_agency_referen', '>', 0);
            })
            ->whereBetween('created_at', [$now->format('Y-m-01 00:00:00'), $now->format('Y-m-d H:i:s')])
            ->count();
        $bonusAgencyConfig = AgencyImportConfig::where('store_id', $request->store->id)->where('end_time', '>=', Carbon::now('Asia/Ho_Chi_Minh')->toDateString())->first();
        $step_reward_received = null;
        $total_final_of_reward = 0;
        $startOfMonth = Carbon::now()->startOfMonth();
        $endOfDay = Carbon::now()->endOfDay();
        $commission_referral = ChangeBalanceAgency::where('store_id', $request->store->id)
            ->where('agency_id', $agency->id)
            ->where('type', BalanceCustomerService::AGENCY_ORDER_COMPLETED_T1)
            ->whereBetween('created_at', [$startOfMonth, $endOfDay])
            ->sum('money');
        if ($bonusAgencyConfig) {
            $step_reward_received = PayRewardAgency::where('store_id', $request->store->id)->where('agency_id',  $agency->id)->where('received_at', '>=', $bonusAgencyConfig->start_time)
                ->where('received_at', '<=', $bonusAgencyConfig->end_time)->where('status', 1)->get();
            // $total_final_of_reward = Order::where('store_id', $request->store->id)
            //     ->where('order_status', StatusDefineCode::COMPLETED)
            //     ->where('payment_status', StatusDefineCode::PAID)
            //     ->where(function ($query) use ($request) {
            //         $query->where('customer_id', $request->customer->id)
            //             ->orWhere('agency_by_customer_referral_id', $request->customer->id)
            //             ->orWhere('agency_by_customer_id', $request->customer->id);
            //     })
            //     ->whereBetween('orders.completed_at', [$bonusAgencyConfig->start_time, $bonusAgencyConfig->end_time])
            //     ->sum('total_final');
            $totalOrders = Order::where('store_id', $request->store->id)
                ->where(function ($query) use ($request) {
                    $query->where('customer_id', $request->customer->id)
                        ->orWhere('agency_by_customer_referral_id', $request->customer->id)
                        ->orWhere('agency_by_customer_id', $request->customer->id);
                })
                ->where('completed_at', '>=', $bonusAgencyConfig->start_time)
                ->where('completed_at', '<=', $bonusAgencyConfig->end_time)
                ->where('order_status', StatusDefineCode::COMPLETED)
                ->where('payment_status', StatusDefineCode::PAID)
                ->sum('total_final');
            $total_received = PayRewardAgency::where('store_id', $request->store->id)
                ->where('agency_id',  $agency->id)->where('received_at', '>=', $bonusAgencyConfig->start_time)
                ->where('received_at', '<', $bonusAgencyConfig->end_time)->sum('total_order');
            $total_final_of_reward = $totalOrders - $total_received;
            if ($total_final_of_reward < 0) {
                $total_final_of_reward = 0;
            }
        }
        $data = [
            'count_in_day' =>  $count_in_day,
            'total_final_in_day' =>  $total_final_in_day,

            'count_in_month' =>  $count_in_month,
            'total_final_in_month' =>  $total_final_in_month,

            'count_in_week' =>  $count_in_week,
            'total_final_in_week' =>  $total_final_in_week,

            'count_in_year' =>  $count_in_year,
            'total_final_in_year' =>  $total_final_in_year,

            'count_in_quarter' =>  $count_in_quarter,
            'total_final_in_quarter' =>  $total_final_in_quarter,

            'total_after_discount_no_bonus_in_day' =>  $total_after_discount_no_bonus_in_day,


            'total_after_discount_no_bonus_in_month' =>  $total_after_discount_no_bonus_in_month,


            'total_after_discount_no_bonus_in_week' =>  $total_after_discount_no_bonus_in_week,


            'total_after_discount_no_bonus_in_year' =>  $total_after_discount_no_bonus_in_year,


            'total_after_discount_no_bonus_in_quarter' =>  $total_after_discount_no_bonus_in_quarter,

            "balance" =>  $agency->balance,
            "commission_referral" => $commission_referral,
            "total_final" =>  $total_final,
            "total_after_discount_no_bonus" =>  $total_after_discount_no_bonus,
            "number_order" => $count,
            "share_agency_ctv" =>  $share_agency_ctv,
            "total_final_ctv" =>  $total_final_ctv,
            "number_order_ctv" => $count_ctv,
            "received_month_bonus" =>  $history == null ? false : true,
            "type_rose" => $configExists->type_rose ?? 0,
            "allow_payment_request" => $configExists->allow_payment_request,
            "type_bonus_period_import" => $configExists->type_bonus_period_import,

            "payment_1_of_month" => $configExists->payment_1_of_month,
            "payment_16_of_month" => $configExists->payment_16_of_month,
            "payment_limit" =>  $configExists->payment_limit,
            "has_payment_request" =>   $payAfter  == null ? false : true,
            "money_payment_request" =>  $payAfter  == null ? null :  $payAfter->money,
            "steps_bonus" => $steps,
            "steps_import" => $steps_import,
            'step_reward_received' => $step_reward_received,
            'total_final_of_reward' => $total_final_of_reward,
            'config_reward' => $bonusAgencyConfig
        ];

        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
            'data' => $data
        ], 200);
    }

    /**
     * Đăng ký dai ly
     * @bodyParam is_agency bool đăng ký hay không hay không (true false)
     */
    public function regAgency(Request $request)
    {

        // $is_agency = filter_var($request->is_agency, FILTER_VALIDATE_BOOLEAN);
        // $request->customer->update([
        //     'is_agency' =>  $is_agency,
        //     'official' => true,
        // ]);

        // if (filter_var($request->is_agency, FILTER_VALIDATE_BOOLEAN) == true) {
        //     $agency = Agency::where('store_id', $request->store->id)
        //         ->where('customer_id', $request->customer->id)->first();

        //     if ($agency == null) {
        //         Agency::create(
        //             [
        //                 'store_id' => $request->store->id,
        //                 'customer_id' =>  $request->customer->id
        //             ]
        //         );
        //     }
        // }

        // PushNotificationUserJob::dispatch(
        //     $request->store->id,
        //     $request->store->user_id,
        //     'Yêu cầu làm đại lý mới',
        //     'Khách hàng ' . $request->customer->name . ' yêu cầu làm đại lý ',
        //     TypeFCM::GET_AGENCY,
        //     $request->customer->id,
        //     null
        // );

        // return response()->json([
        //     'code' => 200,
        //     'success' => true,
        //     'msg_code' => MsgCode::SUCCESS[0],
        //     'msg' => MsgCode::SUCCESS[1],
        // ], 200);
        $agencyRegisterRequest = AgencyRegisterRequest::where('store_id', $request->store->id)
            ->orderBy('id', 'desc')
            ->where('customer_id', $request->customer->id)->first();

        if ($agencyRegisterRequest != null && ($agencyRegisterRequest->status == 0 ||
            $agencyRegisterRequest->status == 3
        )) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::ERROR[0],
                'msg' => 'Bạn đã yêu cầu làm đại lý và đang đợi xử lý',
            ], 400);
        }

        $request->customer->update([
            'is_agency' =>  false,
            'official' => true,
        ]);

        $agency = Agency::where('store_id', $request->store->id)
            ->where('customer_id', $request->customer->id)->first();

        $data =  [
            'store_id' => $request->store->id,
            'customer_id' =>  $request->customer->id,
            'first_and_last_name' => $request->first_and_last_name ??  ($agency != null ? $agency->first_and_last_name : null),
            'cmnd' => $request->cmnd ??  $agency->cmnd,
            'date_range' => $request->date_range ??  $agency->date_range,
            'issued_by' => $request->issued_by ??  $agency->issued_by,
            'front_card' => $request->front_card ??  $agency->front_card,
            'back_card' => $request->back_card ??  $agency->back_card,
            'bank' => $request->bank ??  $agency->bank,
            'account_number' => $request->account_number ??  $agency->account_number,
            'account_name' => $request->account_name ??  $agency->account_name,
            'branch' => $request->branch ??  $agency->branch,
        ];
        if ($agency == null) {
            $agency = Agency::create(
                $data
            );
        } else {
            $agency->update($data);
        }

        $status = 0;

        if ($agencyRegisterRequest != null && $agencyRegisterRequest->status == 1) {
            $status = 3;
        }

        AgencyRegisterRequest::create([
            'status' =>   $status,
            'store_id' => $request->store->id,
            'customer_id' =>  $request->customer->id,
            'agency_id' =>   $agency->id,
        ]);

        PushNotificationUserJob::dispatch(
            $request->store->id,
            $request->store->user_id,
            'Yêu cầu làm đại lý mới',
            'Khách hàng ' . $request->customer->name . ' yêu cầu làm đại lý ',
            TypeFCM::GET_AGENCY,
            $request->customer->id,
            null
        );

        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
        ], 200);
    }

    /**
     * Lấy danh sách GT Agency
     * 
     * @urlParam  store_code required Store code
     * 
     * @bodyParam referral_phone_number 
     * 
     */
    public function getAllReferralPhoneNumberAgency(Request $request)
    {
        $carbon = Carbon::now('Asia/Ho_Chi_Minh');

        $dateFromDay = $carbon->year . '-' . $carbon->month . '-' . $carbon->day . ' 00:00:00';
        $dateToDay = $carbon->year . '-' . $carbon->month . '-' . $carbon->day . ' 23:59:59';
        $cus = Customer::where('store_id', $request->store->id)
            ->where('referral_phone_number', $request->customer->phone_number)
            ->search(request('search'))
            ->paginate(20);

        foreach ($cus as $p) {
            $total_after_discount_no_bonus =   DB::table('orders')->where('store_id', $request->store->id)
                ->where('customer_id', $p->id)
                ->where(function ($query) use ($request) {
                    $query->where("agency_ctv_by_customer_referral_id", "=", $request->customer->id)
                        ->orWhere("agency_ctv_by_customer_id", "=", $request->customer->id);
                })
                ->where('orders.order_status', StatusDefineCode::COMPLETED)
                ->where('orders.payment_status', StatusDefineCode::PAID)
                ->whereDate('created_at', '>=', $request->date_from ?? $dateFromDay)
                ->whereDate('created_at', '<=', $request->date_to ?? $dateToDay)
                ->sum(DB::raw('total_before_discount - combo_discount_amount - product_discount_amount - voucher_discount_amount'));
            $count_orders =   DB::table('orders')->where('store_id', $request->store->id)
                ->where('customer_id', $p->id)
                ->where(function ($query) use ($request) {
                    $query->where("agency_ctv_by_customer_referral_id", "=", $request->customer->id)
                        ->orWhere("agency_ctv_by_customer_id", "=", $request->customer->id);
                })
                ->where('orders.order_status', StatusDefineCode::COMPLETED)
                ->where('orders.payment_status', StatusDefineCode::PAID)
                ->whereDate('created_at', '>=', $request->date_from ?? $dateFromDay)
                ->whereDate('created_at', '<=', ($request->date_to ?? $dateToDay))
                ->count();
            $total_share_agency =   DB::table('orders')->where('store_id', $request->store->id)
                ->where('customer_id', $p->id)
                ->where("agency_ctv_by_customer_id", "=", $request->customer->id)
                ->where('orders.order_status', StatusDefineCode::COMPLETED)
                ->where('orders.payment_status', StatusDefineCode::PAID)
                ->whereDate('created_at', '>=', $request->date_from ?? $dateFromDay)
                ->whereDate('created_at', '<=', $request->date_to ?? $dateToDay)
                ->sum('share_agency');

            $total_share_agency_referen =   DB::table('orders')->where('store_id', $request->store->id)
                ->where('customer_id', $p->id)
                ->where("agency_ctv_by_customer_referral_id", "=", $request->customer->id)
                ->where('orders.order_status', StatusDefineCode::COMPLETED)
                ->where('orders.payment_status', StatusDefineCode::PAID)
                ->whereDate('created_at', '>=', $request->date_from ?? $dateFromDay)
                ->whereDate('created_at', '<=', $request->date_to ?? $dateToDay)
                ->sum('share_agency_referen');

            $p->total_final = $total_after_discount_no_bonus;
            $p->count_orders = $count_orders;
            $p->total_share_agency = $total_share_agency;
            $p->total_share_agency_referen = $total_share_agency_referen;
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'data' =>  $cus,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
        ], 200);
    }

    public function data_total_order(Request $request)
    {

        $agency = Agency::where('store_id', $request->store->id)
            ->where('customer_id', $request->customer->id)->first();

        if ($agency == null) {
            return [];
        }

        $res = Order::where('store_id', $request->store->id)
            ->where('agency_ctv_by_customer_id', $request->customer->id)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->selectRaw('year(created_at) year, month(created_at) month, sum(total_final) total_final, sum(share_agency) share_agency')
            ->groupBy('year', 'month')
            ->orderBy('year', 'desc')
            ->get();

        $data = [];

        foreach ($res  as $itemOrder) {
            $history = HistoryBonusAgency::where('store_id', $request->store->id)
                ->where('agency_id', $agency->id)
                ->where('year', $itemOrder->year)
                ->where('month', $itemOrder->month)->first();

            //////////////

            $money_current = 0;
            $money_bonus_current = 0;

            $steps = AgencyBonusStep::where('store_id', $request->store->id)->orderBy('limit', 'desc')->get();
            $configExists = AgencyConfig::where(
                'store_id',
                $request->store->id
            )->first();

            //Theo doanh số
            if ($configExists->type_rose == 0) {
                $money_current = $itemOrder->total_final ?? 0;
            }


            foreach ($steps as $step) {
                if ($step->limit <= $money_current) {
                    $use_limit_config = $step->limit;
                    $money_bonus_current = $step->bonus;
                    break;
                }
            }

            //////////////  //////////////  //////////////

            array_push($data, [
                'year' => $itemOrder->year,
                'month' => $itemOrder->month,
                'total_final' => $itemOrder->total_final ?? 0,
                'share_agency' => $itemOrder->share_agency ?? 0,
                'awarded' => $history == null ? false : true,
                'money_bonus_rewarded' =>  $history->money_bonus_rewarded ?? 0,
                'money_bonus_current' =>  $money_bonus_current  ?? 0
            ]);
        }

        return     $data;
    }

    /**
     * Báo cáo thưởng  bậc thang
     */
    public function list_bonus_with_month(Request $request)
    {

        $data = $this->data_total_order($request);

        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
            'data' =>     $this->paginate($data)
        ], 200);
    }

    /**
     * Nhận thưởng tháng
     * @bodyParam month Tháng muốn nhận VD: 2
     * @bodyParam year Năm muốn nhận VD: 2012
     */
    public function take_bonus(Request $request)
    {

        $agency = Agency::where('store_id', $request->store->id)
            ->where('customer_id', $request->customer->id)->first();

        if ($agency == null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_REGISTERED_COLLABORATOR[0],
                'msg' => MsgCode::NOT_REGISTERED_COLLABORATOR[1],
            ], 400);
        }

        if ($request->month < 1 || $request->month > 12) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::INVALID_MONTH[0],
                'msg' => MsgCode::INVALID_MONTH[1],
            ], 400);
        }

        if ($request->year == null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::INVALID_YEAR[0],
                'msg' => MsgCode::INVALID_YEAR[1],
            ], 400);
        }

        $data = $this->data_total_order($request);


        $has_time = false;
        $data_money_agency = null;
        foreach ($data as $item) {
            if ($item["year"] == $request->year  && $item["month"] == $request->month) {
                $has_time = true;
                $data_money_agency  = $item;
                break;
            }
        }

        if ($has_time  == false) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NO_ORDERS_IN_TIME[0],
                'msg' => MsgCode::NO_ORDERS_IN_TIME[1],
            ], 400);
        }

        $steps = AgencyBonusStep::where('store_id', $request->store->id)->orderBy('limit', 'desc')->get();

        $configExists = AgencyConfig::where(
            'store_id',
            $request->store->id
        )->first();

        if (count($steps) == 0 ||    $configExists  == null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NO_BONUS_INSTALLED[0],
                'msg' => MsgCode::NO_BONUS_INSTALLED[1],
            ], 400);
        }

        $use_limit_config = null;
        $money_current = null;
        $money_bonus_rewarded = 0;


        //Theo doanh số
        if ($configExists->type_rose == 0) {
            $money_current = $data_money_agency["total_final"];
        }


        foreach ($steps as $step) {
            if ($step->limit <= $money_current) {
                $use_limit_config = $step->limit;
                $money_bonus_rewarded = $step->bonus;
                break;
            }
        }

        if ($use_limit_config == null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_ELIGIBLE_REWARD[0],
                'msg' => MsgCode::NOT_ELIGIBLE_REWARD[1],
            ], 400);
        }

        $history = HistoryBonusAgency::where('store_id', $request->store->id)
            ->where('agency_id', $agency->id)
            ->where('year', $request->year)
            ->where('month', $request->month)->first();

        if ($history != null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::RECEIVED_MONTH_BONUS[0],
                'msg' => MsgCode::RECEIVED_MONTH_BONUS[1],
            ], 400);
        }

        $history = HistoryBonusAgency::create([
            "store_id" => $request->store->id,
            "agency_id" => $agency->id,
            "year" => $request->year,
            "month" => $request->month,
            "money_bonus_rewarded" => $money_bonus_rewarded,
            "limit" => $use_limit_config,
        ]);

        BalanceCustomerService::change_balance_collaborator(
            $request->store->id,
            $request->customer->id,
            BalanceCustomerService::BONUS_MONTH,
            $money_bonus_rewarded,
            $history->id,
            $request->month . "/" . $request->year
        );

        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
        ], 200);
    }

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function getTotalRevenueAgency(Request $request)
    {
        $dateFrom = request('date_from');
        $dateTo = request('date_to');

        if (empty($dateFrom)) {
            $dateFrom = Carbon::now('Asia/Ho_Chi_Minh')->startOfMonth()->format('Y-m-d H:i:s');
        } else {
            $dateFrom = Carbon::parse($dateFrom, 'Asia/Ho_Chi_Minh')->startOfDay()->format('Y-m-d H:i:s');
        }

        if (empty($dateTo)) {
            $dateTo = Carbon::now('Asia/Ho_Chi_Minh')->endOfDay()->format('Y-m-d H:i:s');
        } else {
            $dateTo = Carbon::parse($dateTo, 'Asia/Ho_Chi_Minh')->endOfDay()->format('Y-m-d H:i:s');
        }

        $agency = Agency::where('store_id', $request->store->id)->where('customer_id', request('agency_id'))->first();
        // $orders = Order::where('store_id', $request->store->id)
        //     ->where(function ($query) {
        //         $query->where('customer_id', request('agency_id'));
        //         // ->orWhere('agency_by_customer_referral_id', request('agency_id'))
        //         // ->orWhere('agency_by_customer_id', request('agency_id'));
        //     })
        //     ->where('orders.completed_at', '>=',  $dateFrom)
        //     ->where('orders.completed_at', '<=', $dateTo)
        //     ->where('orders.order_status', StatusDefineCode::COMPLETED)
        //     ->where('orders.payment_status', StatusDefineCode::PAID)
        //     ->get();
        $baseQuery = Order::where('store_id', $request->store->id)
            ->where('orders.completed_at', '>=', $dateFrom)
            ->where('orders.completed_at', '<=', $dateTo)
            ->where('orders.order_status', StatusDefineCode::COMPLETED)
            ->where('orders.payment_status', StatusDefineCode::PAID);
        $orders = clone $baseQuery;
        $orders = $orders->where(function ($query) use ($agency) {
            $query->where('customer_id', $agency->customer_id);
            // ->orWhere('agency_by_customer_referral_id', $agency->customer_id)
            // ->orWhere('agency_by_customer_id', $agency->customer_id);
        })->get();

        $orderCtvAgency = clone $baseQuery;
        $orderCtvAgency = $orderCtvAgency->where(function ($query) use ($agency) {
            $query->where('collaborator_by_customer_id', '!=', null)
                ->where('agency_ctv_by_customer_referral_id', $agency->customer_id);
        })->get();

        $orderAgencyReferral = clone $baseQuery;
        $orderAgencyReferral = $orderAgencyReferral->where(function ($query) use ($agency) {
            $query->where('agency_by_customer_referral_id', $agency->customer_id)
                ->where('agency_by_customer_id', '!=', null);
        })->get();

        $totalSumAll = 0;
        if ($agency) {
            $agency->orders_count = $orders->count();
            $agency->sum_total_final = $orders->sum('total_before_discount');
            $agency->sum_share_agency = $agency->balance;
            $agency->total_after_discount_no_bonus = $orders->sum(function ($order) {
                return $order->total_before_discount - $order->combo_discount_amount - $order->product_discount_amount - $order->voucher_discount_amount - ($order->discount ?? 0);
            });
            $agency->total_before_discount = $orders->sum('total_before_discount');
            $agency->combo_discount_amount = $orders->sum('combo_discount_amount');
            $agency->product_discount_amount = $orders->sum('product_discount_amount');
            $agency->voucher_discount_amount = $orders->sum('voucher_discount_amount');
            $agency->sum_discount = $orders->sum(function ($order) {
                return $order->is_order_for_customer == 1 ? 0 : ($order->total_final_before_override - $order->total_final);
            });
            $agency->sum_commission_agency_refferal = $orderAgencyReferral->sum('share_agency_referen');
            $agency->sum_commission_customer_refferal = $orderCtvAgency->sum('share_agency_referen');
            $totalSumAll = $agency->sum_total_final;
        } else {
            return response()->json([
                'code' => 404,
                'success' => false,
                'msg_code' => "NOT_FOUND",
                'msg' => "Đại lí không tồn tại",
            ], 404);
        }

        if ($agency) {
            $customerRefferal =  Customer::sortByRelevance(true)
                ->where(
                    'customers.store_id',
                    $request->store->id
                )
                ->where('referral_phone_number', $agency->customer->phone_number)
                ->leftJoin('orders', function ($join) use ($dateFrom, $dateTo) {
                    $join->on(function ($query) {
                        $query->on('customers.id', '=', 'orders.agency_by_customer_referral_id')
                            ->orOn('customers.id', '=', 'orders.agency_by_customer_id')
                            ->orOn('customers.id', '=', 'orders.customer_id');
                    })
                        // ->whereBetween('orders.completed_at', [$dateFrom, $dateTo])
                        ->where(function ($query) use ($dateFrom, $dateTo) {
                            $query->where(function ($subQuery) use ($dateFrom, $dateTo) {
                                $subQuery->where('orders.from_pos', true)
                                    ->whereBetween('orders.created_at', [$dateFrom, $dateTo]);
                            })->orWhere(function ($subQuery) use ($dateFrom, $dateTo) {
                                $subQuery->where('orders.from_pos', false)
                                    ->whereBetween('orders.completed_at', [$dateFrom, $dateTo]);
                            });
                        })
                        ->where('orders.order_status', StatusDefineCode::COMPLETED)
                        ->where('orders.payment_status', StatusDefineCode::PAID);
                })
                ->selectRaw('customers.*, count(orders.id) as orders_count, sum(orders.total_before_discount) as sum_total_final, sum(total_before_discount - combo_discount_amount - product_discount_amount - voucher_discount_amount - COALESCE(discount, 0)) as total_after_discount_no_bonus, total_before_discount, combo_discount_amount, product_discount_amount, voucher_discount_amount, sum(total_final_before_override - total_final) as sum_discount')
                ->groupBy('customers.id')
                ->get();
            $sumTotalFinalCustomerRefferal = $customerRefferal->sum('sum_total_final');
            $totalSumAll += $sumTotalFinalCustomerRefferal;
            $agency->customer_referred = $customerRefferal;
            $agency->total_sum_all = $totalSumAll;
        }


        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
            'data' => $agency ?? [],
        ], 200);
    }
}
