<?php

namespace App\Http\Controllers\Api\Customer;

use App\Helper\StatusDefineCode;
use App\Helper\TypeFCM;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PaymentMethod\PayController;
use App\Jobs\PushNotificationUserJob;
use App\Models\BonusCollaborator;
use App\Models\Collaborator;
use App\Models\CollaboratorBonusStep;
use App\Models\CollaboratorsConfig;
use App\Models\MsgCode;
use App\Models\Order;
use App\Models\PayCollaborator;
use App\Models\PayRewardCollaborator;
use App\Services\BalanceCustomerService;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * @group  Customer/Thanh toán tiền hoa hồng
 */
class CustomerCollaboratorPayController extends Controller
{
    /**
     * Yêu cầu thanh toán
     */
    public function request_payment(Request $request)
    {

        $collaborator = Collaborator::where('store_id', $request->store->id)
            ->where('customer_id', $request->customer->id)->first();

        if ($collaborator == null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_REGISTERED_COLLABORATOR[0],
                'msg' => MsgCode::NOT_REGISTERED_COLLABORATOR[1],
            ], 400);
        }

        $configExists = CollaboratorsConfig::where(
            'store_id',
            $request->store->id
        )->first();

        if ($configExists  == null || $configExists->payment_limit == null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::STORE_HAS_NOT_CONFIGURED[0],
                'msg' => MsgCode::STORE_HAS_NOT_CONFIGURED[1],
            ], 400);
        }

        if ($configExists->allow_payment_request == false) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::STORE_NOT_ALLOW_YOU_TO_MAKE_PAYMENTS[0],
                'msg' => MsgCode::STORE_NOT_ALLOW_YOU_TO_MAKE_PAYMENTS[1],
            ], 400);
        }

        if ($collaborator->balance < $configExists->payment_limit) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::LIMIT_NOT_REACHED[0],
                'msg' => MsgCode::LIMIT_NOT_REACHED[1],
            ], 400);
        }

        $payAfter = PayCollaborator::where('store_id', $request->store->id)
            ->where('collaborator_id',  $collaborator->id)->where('status', 0)->first();

        if ($payAfter  != null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::HAVE_AN_UNPAID_REQUEST[0],
                'msg' => MsgCode::HAVE_AN_UNPAID_REQUEST[1],
            ], 400);
        }

        if ($collaborator->balance  <= 0) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::HAVE_AN_UNPAID_REQUEST[0],
                'msg' =>  'Số dư không có để yêu cầu thanh toán'
            ], 400);
        }



        PushNotificationUserJob::dispatch(
            $request->store->id,
            $request->store->user_id,
            'Yêu cầu thanh toán',
            'CTV ' . $request->customer->name . ' vừa gửi yêu cầu thanh toán ',
            TypeFCM::REQUEST_PAY_CTV,
            $request->customer->id,
            null
        );

        PayCollaborator::create([
            "store_id" => $request->store->id,
            "collaborator_id"  =>  $collaborator->id,
            "money"  =>  $collaborator->balance,
            "status"  => 0,
            "from"  => 0,
        ]);

        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
        ], 200);
    }

    public function request_payment_reward(Request $request)
    {
        $collaborator = Collaborator::where('store_id', $request->store->id)
            ->where('customer_id', $request->customer->id)->first();
        $bonusCollaboratorConfig = BonusCollaborator::where('store_id', $request->store->id)->where('end_time', '>=', Carbon::now('Asia/Ho_Chi_Minh')->toDateString())->first();
        if (!$bonusCollaboratorConfig) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_REGISTERED_COLLABORATOR[0],
                'msg' => "Thời gian thi đua không tồn tại",
            ], 400);
        }

        if ($collaborator == null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_REGISTERED_COLLABORATOR[0],
                'msg' => MsgCode::NOT_REGISTERED_COLLABORATOR[1],
            ], 400);
        }

        $totalOrders = Order::where('store_id', $request->store->id)
            ->where(function ($query) use ($request) {
                $query->where('customer_id', $request->customer->id)
                    ->orWhere('collaborator_by_customer_referral_id', $request->customer->id)
                    ->orWhere('collaborator_by_customer_id', $request->customer->id);
            })
            ->where('completed_at', '>=', $bonusCollaboratorConfig->start_time)
            ->where('completed_at', '<', $bonusCollaboratorConfig->end_time)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->sum('total_final');

        $step = CollaboratorBonusStep::where('store_id', $request->store->id)->where('limit', '<=', $totalOrders)->where('bonus', $request->limit_request)->first();
        if (!$step) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_REGISTERED_COLLABORATOR[0],
                'msg' => "Yêu cầu thanh toán thưởng không hợp lệ",
            ], 400);
        }

        if ($totalOrders < $step->limit) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::LIMIT_NOT_REACHED[0],
                'msg' => "Chưa đạt thi đua",
            ], 400);
        }

        // $payAfter = PayRewardCollaborator::where('store_id', $request->store->id)
        //     ->where('collaborator_id',  $collaborator->id)->where('status', 0)->first();

        // if ($payAfter  != null) {
        //     return response()->json([
        //         'code' => 400,
        //         'success' => false,
        //         'msg_code' => MsgCode::HAVE_AN_UNPAID_REQUEST[0],
        //         'msg' => MsgCode::HAVE_AN_UNPAID_REQUEST[1],
        //     ], 400);
        // }

        $total_received = PayRewardCollaborator::where('store_id', $request->store->id)
            ->where('collaborator_id',  $collaborator->id)->where('received_at', '>=', $bonusCollaboratorConfig->start_time)
            ->where('received_at', '<', $bonusCollaboratorConfig->end_time)->sum('total_order');
            $totalOrderCurrent = $totalOrders - $total_received;

        // $step_received = PayRewardCollaborator::where('store_id', $request->store->id)->where('collaborator_id',  $collaborator->id)->where('received_at', '>=', $bonusCollaboratorConfig->start_time)
        //     ->where('received_at', '<', $bonusCollaboratorConfig->end_time)->where('money', $request->limit_request)->where('status', 1)->first();

        // if ($step_received) {
        //     return response()->json([
        //         'code' => 400,
        //         'success' => false,
        //         'msg_code' => MsgCode::LIMIT_NOT_REACHED[0],
        //         'msg' => "Đã nhận mốc phần thưởng này",
        //     ], 400);
        // }

        if ($totalOrderCurrent >= $step->limit) {
            $pay = PayRewardCollaborator::create([
                "store_id" => $request->store->id,
                "collaborator_id"  =>  $collaborator->id,
                "money"  =>  $request->limit_request,
                "status"  => 1,
                "received_at"  => Carbon::now()->format('Y-m-d H:i:s'),
                "total_order" => $step->limit
            ]);

            BalanceCustomerService::change_balance_collaborator(
                $request->store->id,
                $collaborator->customer_id,
                BalanceCustomerService::CTV_REWARD,
                $request->limit_request,
                $pay->id,
                "Nhận thưởng thi đua",
                "Nhận thưởng thi đua",
            );
        } else {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::LIMIT_NOT_REACHED[0],
                'msg' => "Chưa đạt thi đua",
            ], 400);
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
        ], 200);
    }
}
