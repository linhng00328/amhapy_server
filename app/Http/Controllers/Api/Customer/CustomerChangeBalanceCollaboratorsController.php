<?php

namespace App\Http\Controllers\Api\Customer;

use App\Http\Controllers\Controller;
use App\Models\ChangeBalanceCollaborator;
use App\Models\Collaborator;
use App\Models\MsgCode;

use Illuminate\Http\Request;

/**
 * @group  Customer/Lịch sử thay đổi số dư
 */
class CustomerChangeBalanceCollaboratorsController extends Controller
{

    /**
     * Lịch sử thay đổi số dư
     * @urlParam  store_code required Store code cần lấy.
     */
    public function getAll(Request $request)
    {

        $collaborator  = Collaborator::where('store_id', $request->store->id)->where('customer_id', $request->customer->id)->first();

        $histories = ChangeBalanceCollaborator::where('change_balance_collaborators.store_id', $request->store->id)
            ->leftJoin('orders', 'change_balance_collaborators.references_value', '=', 'orders.order_code')
            ->leftJoin('customers', 'orders.customer_id', '=', 'customers.id')
            ->selectRaw('orders.*, change_balance_collaborators.*, customers.name as customer_order_name')
            ->where('change_balance_collaborators.collaborator_id',  $collaborator->id)
            ->orderBy('change_balance_collaborators.created_at', 'desc')
            ->paginate(20);

        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
            'data' =>    $histories,
        ], 200);
    }
}
