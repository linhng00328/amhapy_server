<?php

namespace App\Http\Controllers\Api\Customer;

use App\Helper\StatusDefineCode;
use App\Helper\TypeFCM;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PaymentMethod\PayController;
use App\Jobs\PushNotificationUserJob;
use App\Models\Agency;
use App\Models\AgencyBonusStep;
use App\Models\AgencyConfig;
use App\Models\AgencyImportConfig;
use App\Models\AgencyImportStep;
use App\Models\AgencysConfig;
use App\Models\BonusAgency;
use App\Models\MsgCode;
use App\Models\Order;
use App\Models\PayAgency;
use App\Models\PayRewardAgency;
use App\Services\BalanceCustomerService;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * @group  Customer/Thanh toán tiền hoa hồng
 */
class CustomerAgencyPayController extends Controller
{
    /**
     * Yêu cầu thanh toán
     */
    public function request_payment(Request $request)
    {

        $agency = Agency::where('store_id', $request->store->id)
            ->where('customer_id', $request->customer->id)->first();

        if ($agency == null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_REGISTERED_AGENCY[0],
                'msg' => MsgCode::NOT_REGISTERED_AGENCY[1],
            ], 400);
        }

        // $configExists = AgencyConfig::where(
        //     'store_id',
        //     $request->store->id
        // )->first();

        // if ($configExists  == null || $configExists->payment_limit == null) {
        //     return response()->json([
        //         'code' => 400,
        //         'success' => false,
        //         'msg_code' => MsgCode::STORE_HAS_NOT_AGENCY_CONFIGURED[0],
        //         'msg' => MsgCode::STORE_HAS_NOT_AGENCY_CONFIGURED[1],
        //     ], 400);
        // }

        // if ($configExists->allow_payment_request == false) {
        //     return response()->json([
        //         'code' => 400,
        //         'success' => false,
        //         'msg_code' => MsgCode::STORE_NOT_ALLOW_YOU_TO_MAKE_PAYMENTS[0],
        //         'msg' => MsgCode::STORE_NOT_ALLOW_YOU_TO_MAKE_PAYMENTS[1],
        //     ], 400);
        // }

        // if ($agency->balance < $configExists->payment_limit) {
        //     return response()->json([
        //         'code' => 400,
        //         'success' => false,
        //         'msg_code' => MsgCode::LIMIT_NOT_REACHED[0],
        //         'msg' => MsgCode::LIMIT_NOT_REACHED[1],
        //     ], 400);
        // }

        $payAfter = PayAgency::where('store_id', $request->store->id)
            ->where('agency_id',  $agency->id)->where('status', 0)->first();

        if ($payAfter  != null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::HAVE_AN_UNPAID_REQUEST[0],
                'msg' => MsgCode::HAVE_AN_UNPAID_REQUEST[1],
            ], 400);
        }

        if ($agency->balance  <= 0) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::HAVE_AN_UNPAID_REQUEST[0],
                'msg' =>  'Số dư không có để yêu cầu thanh toán'
            ], 400);
        }

        PayAgency::create([
            "store_id" => $request->store->id,
            "agency_id"  =>  $agency->id,
            "money"  =>  $agency->balance,
            "status"  => 0,
            "from"  => 0,
        ]);

        PushNotificationUserJob::dispatch(
            $request->store->id,
            $request->store->user_id,
            'Yêu cầu thanh toán',
            'Đại lý ' . $request->customer->name . ' vừa gửi yêu cầu thanh toán ',
            TypeFCM::REQUEST_PAY_AGENCY,
            $request->customer->id,
            null
        );

        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
        ], 200);
    }

    public function request_payment_reward(Request $request)
    {
        $agency = Agency::where('store_id', $request->store->id)
            ->where('customer_id', $request->customer->id)->first();

        $bonusAgencyConfig = AgencyImportConfig::where('store_id', $request->store->id)->where('end_time', '>=', Carbon::now('Asia/Ho_Chi_Minh')->toDateString())->first();
        if (!$bonusAgencyConfig) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_REGISTERED_COLLABORATOR[0],
                'msg' => "Thời gian thi đua không tồn tại",
            ], 400);
        }

        if ($agency == null) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_REGISTERED_COLLABORATOR[0],
                'msg' => MsgCode::NOT_REGISTERED_COLLABORATOR[1],
            ], 400);
        }

        $totalOrders = Order::where('store_id', $request->store->id)
            ->where(function ($query) use ($request) {
                $query->where('customer_id', $request->customer->id)
                    ->orWhere('agency_by_customer_referral_id', $request->customer->id)
                    ->orWhere('agency_by_customer_id', $request->customer->id);
            })
            ->where('completed_at', '>=', $bonusAgencyConfig->start_time)
            ->where('completed_at', '<=', $bonusAgencyConfig->end_time)
            ->where('order_status', StatusDefineCode::COMPLETED)
            ->where('payment_status', StatusDefineCode::PAID)
            ->sum('total_final');
        $step = AgencyImportStep::where('store_id', $request->store->id)->where('limit', '<=', $totalOrders)->where('bonus', $request->limit_request)->first();
        if (!$step) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::NOT_REGISTERED_COLLABORATOR[0],
                'msg' => "Yêu cầu thanh toán thưởng không hợp lệ",
            ], 400);
        }

        if ($totalOrders < $step->limit) {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::LIMIT_NOT_REACHED[0],
                'msg' => "Chưa đạt thi đua",
            ], 400);
        }
        $total_received = PayRewardAgency::where('store_id', $request->store->id)
            ->where('agency_id',  $agency->id)->where('received_at', '>=', $bonusAgencyConfig->start_time)
            ->where('received_at', '<', $bonusAgencyConfig->end_time)->sum('total_order');
        $totalOrderCurrent = $totalOrders - $total_received;

        // $step_received = PayRewardAgency::where('store_id', $request->store->id)->where('agency_id',  $agency->id)->where('received_at', '>=', $bonusAgencyConfig->start_time)
        //     ->where('received_at', '<', $bonusAgencyConfig->end_time)->where('money', $request->limit_request)->where('status', 1)->first();

        // if ($step_received) {
        //     return response()->json([
        //         'code' => 400,
        //         'success' => false,
        //         'msg_code' => MsgCode::LIMIT_NOT_REACHED[0],
        //         'msg' => "Đã nhận mốc phần thưởng này",
        //     ], 400);
        // }

        if ($totalOrderCurrent >= $step->limit) {
            $pay = PayRewardAgency::create([
                "store_id" => $request->store->id,
                "agency_id"  =>  $agency->id,
                "money"  =>  $request->limit_request,
                "status"  => 1,
                "received_at"  => Carbon::now()->format('Y-m-d H:i:s'),
                "total_order" => $step->limit
            ]);

            BalanceCustomerService::change_balance_agency(
                $request->store->id,
                $agency->customer_id,
                BalanceCustomerService::AGENCY_REWARD,
                $request->limit_request,
                $pay->id,
                "Nhận thưởng thi đua",
                "Nhận thưởng thi đua",
            );
        } else {
            return response()->json([
                'code' => 400,
                'success' => false,
                'msg_code' => MsgCode::LIMIT_NOT_REACHED[0],
                'msg' => "Chưa đạt thi đua",
            ], 400);
        }

        return response()->json([
            'code' => 200,
            'success' => true,
            'msg_code' => MsgCode::SUCCESS[0],
            'msg' => MsgCode::SUCCESS[1],
        ], 200);
    }
}
