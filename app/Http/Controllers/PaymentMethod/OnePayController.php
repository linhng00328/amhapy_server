<?php

namespace App\Http\Controllers\PaymentMethod;

use App\Helper\Helper;
use App\Helper\StatusDefineCode;
use App\Helper\TypeFCM;
use App\Http\Controllers\Controller;
use App\Jobs\PushNotificationUserJob;
use App\Models\LinkBackPay;
use App\Models\Order;
use App\Models\OrderRecord;
use App\Models\PaymentMethod;
use App\Models\StatusPaymentHistory;
use Exception;
use Illuminate\Http\Request;

function cryptoJsHmacSha256(string $data, string $key): string
{
    $sign = hash_hmac("sha256", $data, $key, false);
    return strtoupper($sign);
}

function cryptoJsHexParse(string $hexString): string
{
    return hex2bin($hexString);
}


/**
 * @group  Customer/thanh toán onpay
 */
class OnePayController extends Controller
{


    public function index(Request $request)
    {
        return $this->pay($request);
    }


    public function getDataPay(Request $request)
    {

        $methodExists = PaymentMethod::where('store_id', $request->store->id)
            ->where('method_id', 3)->first();

        $config = null;

        $merchant = "";
        $hascode = "";
        $access_code = "";
        if ($methodExists && isset($methodExists->json_data)) {
            $config = json_decode($methodExists->json_data);

            $merchant  =  $config->merchant;
            $hascode =  $config->hascode;
            $access_code =  $config->access_code;
        }

        return [
            "merchant" => $merchant,
            "hascode" => $hascode,
            "access_code" => $access_code,
        ];
    }

    public function generateStringToHash($array)
    {
        $stringToHash = "";
        foreach ($array as $key => $value) {
            $pref4 = substr($key, 0, 4);
            $pref5 = substr($key, 0, 5);
            if ($pref4 == "vpc_" || $pref5 == "user_") {
                if ($key != "vpc_SecureHashType" && $key != "vpc_SecureHash") {
                    if (strlen($value) > 0) {
                        if (strlen($stringToHash) > 0) {
                            $stringToHash = $stringToHash . "&";
                        }
                        $stringToHash = $stringToHash . $key . "=" . $value;
                    }
                }
            }
        }
        return $stringToHash;
    }

    public function generateSecureHash($stringToHash, $merchantHashCode)
    {
        $merchantHex = cryptoJsHexParse($merchantHashCode);
        return cryptoJsHmacSha256($stringToHash, $merchantHex);
    }

    public function sendHttpRequest($apiUrl)
    {
        dd($apiUrl);
        // Create a stream
        $curl = curl_init($apiUrl);
        curl_setopt($curl, CURLOPT_HEADER, true);  // we want headers
        $response = curl_exec($curl);
        curl_close($curl);
    }

    public function createV1(Request $request)
    {
        $host = $request->getSchemeAndHttpHost();

        $order_code = $request->order->order_code;
        $store_code = $request->store->store_code;


        $dataPay = $this->getDataPay($request);
        session(['cost_id' => $request->id]);
        session(['url_prev' => url()->previous()]);

        $env =  env("APP_ENV", "local");

        $vpc_Url = $env == "local" ? "https://mtf.onepay.vn/paygate/vpcpay.op" : "https://onepay.vn/paygate/vpcpay.op";

        // $vpc_Url = "https://mtf.onepay.vn/paygate/vpcpay.op";

        $vpc_AccessCode = $dataPay['access_code'] ?? "";
        $vpc_Merchant = $dataPay['merchant'] ?? "";
        $SECURE_SECRET = $dataPay['hascode'] ?? "";

        $vpc_Amount = $request->order->total_final * 100;
        $vpc_Locale = 'vn';
        $vpc_IpAddr = request()->ip();
        $vnp_Returnurl = $host . "/api/customer/$store_code/purchase/return/one_pay";

        $vpcMerchantTxnRef = Helper::generateRandomNum(20);
        $merchantParam = [
            "vpc_Version" => "2",
            "vpc_Currency" => "VND",
            "vpc_Command" => "pay",
            "vpc_AccessCode" => $vpc_AccessCode,
            "vpc_MerchTxnRef" => $vpcMerchantTxnRef,
            "vpc_Merchant" => $vpc_Merchant,
            "vpc_Locale" => "vn",
            "vpc_ReturnURL" => $vnp_Returnurl,
            "vpc_OrderInfo" => $order_code,
            "vpc_Amount" => $vpc_Amount,
            "vpc_TicketNo" => $vpc_IpAddr,
            "AgainLink" => $vnp_Returnurl,
            "Title" => "Thanh toán đơn hàng " . $order_code,
            // "vpc_Customer_Phone" => "84338641051",
            // "vpc_Customer_Email" => "nguyenngan16082001@gmail.com",
            // "vpc_Customer_Id" => "test11",
        ];
        ksort($merchantParam);
        $stringToHash = $this->generateStringToHash($merchantParam);
        $secureHash = $this->generateSecureHash($stringToHash, $SECURE_SECRET);
        $merchantParam['vpc_SecureHash'] = $secureHash;
        $requestUrl = $vpc_Url . "?" . http_build_query($merchantParam);

        return redirect($requestUrl);
    }

    public function create(Request $request)
    {
        $host = $request->getSchemeAndHttpHost();

        $order_code = $request->order->order_code;
        $store_code = $request->store->store_code;


        $dataPay = $this->getDataPay($request);

        session(['cost_id' => $request->id]);
        session(['url_prev' => url()->previous()]);

        $env =  env("APP_ENV", "local");

        $vpc_Url = $env == "local" ? "https://mtf.onepay.vn/paygate/vpcpay.op" : "https://onepay.vn/paygate/vpcpay.op";


        $vpc_AccessCode = $dataPay['access_code'] ?? "";
        $vpc_Merchant = $dataPay['merchant'] ?? "";
        $SECURE_SECRET = $dataPay['hascode'] ?? "";

        $vpc_Amount = $request->order->total_final * 100;
        $vpc_Locale = 'vn';
        $vpc_IpAddr = request()->ip();
        $vnp_Returnurl = $host . "/api/customer/$store_code/purchase/return/one_pay";

        $inputData = array(
            "vpc_Version" => "2",
            "vpc_Currency" => "VND",
            "vpc_Command" => "pay",
            "vpc_AccessCode" => $vpc_AccessCode,
            "vpc_Merchant" => $vpc_Merchant,
            "vpc_Locale" =>  $vpc_Locale,

            "vpc_ReturnURL" =>  $vnp_Returnurl,
            "vpc_MerchTxnRef" => Helper::generateRandomNum(20),
            "vpc_OrderInfo" =>  $order_code,
            "vpc_Amount" => $vpc_Amount,
            "vpc_TicketNo" => $vpc_IpAddr,
            "Title" => "Thanh toán đơn hàng " . $order_code,
            "AgainLink" => $vnp_Returnurl
        );

        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . $key . "=" . $value;
            } else {
                $hashdata .= $key . "=" . $value;
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vpc_Url = $vpc_Url . "?" . $query;


        $stringHashData = "";

        // sort all the incoming vpc response fields and leave out any with no value
        foreach ($inputData as $key => $value) {
            //        if ($key != "vpc_SecureHash" or strlen($value) > 0) {
            //            $stringHashData .= $value;
            //        }
            //      *****************************chỉ lấy các tham số bắt đầu bằng "vpc_" hoặc "user_" và khác trống và không phải chuỗi hash code trả về*****************************
            if ($key != "vpc_SecureHash" && (strlen($value) > 0) && ((substr($key, 0, 4) == "vpc_") || (substr($key, 0, 5) == "user_"))) {
                $stringHashData .= $key . "=" . $value . "&";
            }
        }
        //  *****************************Xóa dấu & thừa cuối chuỗi dữ liệu*****************************
        $stringHashData = rtrim($stringHashData, "&");



        $vnpSecureHash =   strtoupper(hash_hmac('SHA256', $stringHashData, pack('H*', $SECURE_SECRET)));
        $vpc_Url .= 'vpc_SecureHash=' . $vnpSecureHash;




        return redirect($vpc_Url);
    }

    protected function verifySecureHash($queriesParamMap, $merchantHashCode)
    {
        $merchantHash = $queriesParamMap['vpc_SecureHash'];
        ksort($queriesParamMap);
        $stringToHash = $this->generateStringToHash($queriesParamMap);
        $onePayHash = $this->generateSecureHash($stringToHash, $merchantHashCode);
        if ($merchantHash != $onePayHash) {
            return false;
        } else {
            return true;
        }
    }

    public function returnV1(Request $request)
    {
        $dataPay = $this->getDataPay($request);
        $SECURE_SECRET = $dataPay['hascode'] ?? "";

        $vpc_Txn_Secure_Hash = $_GET["vpc_SecureHash"];
        ksort($_GET);

        $inputData = $_GET;

        if (!$this->verifySecureHash($inputData, $SECURE_SECRET)) {
            return response()->json(['success' => false, 'message' => 'Secure hash verification failed.'], 400);
        }
        unset($_GET["vpc_SecureHash"]);

        $orderId = $inputData['vpc_MerchTxnRef'] ?? null;
        $transactionStatus = $inputData['vpc_TxnResponseCode'] ?? null;
        if (strlen($SECURE_SECRET) > 0 && $orderId && $transactionStatus === '0') {
            $historyExists = StatusPaymentHistory::where(
                'order_code',
                $inputData['vpc_OrderInfo']
            )->orderBy('id', 'desc')->first();

            $orderExists = Order::where(
                'order_code',
                $inputData['vpc_OrderInfo']
            )->first();

            if (empty($historyExists)) {
                StatusPaymentHistory::create(
                    [
                        "order_code" => $inputData['vpc_OrderInfo'],
                        "transaction_no" => $inputData['vpc_TransactionNo'],
                        "amount" => ($inputData['vpc_Amount'] != null && $inputData['vpc_Amount'] > 0) ? $inputData['vpc_Amount'] / 100 : 0,
                        "bank_code" =>  "",
                        "card_type" =>  $inputData['vpc_Card'] ?? "",
                        "order_info" => $inputData['vpc_OrderInfo'],
                        "pay_date" => Helper::getTimeNowString(),
                        "response_code" => $inputData['vpc_TxnResponseCode'] ?? "",
                        "key_code_customer" => "",
                    ]
                );

                if (!empty($orderExists)) {

                    PushNotificationUserJob::dispatch(
                        $request->store->id,
                        $request->store->user_id,
                        'Shop ' . $orderExists->store->name,
                        'Đơn hàng ' . $orderExists->order_code . ' đã được thanh toán',
                        TypeFCM::CUSTOMER_PAID,
                        $orderExists->order_code,
                        null
                    );

                    if ($orderExists->customer_id != null) {
                        StatusDefineCode::saveOrderStatus(
                            $request->store->id,
                            $orderExists->customer_id,
                            $orderExists->id,
                            "Đã thanh toán đơn hàng qua OnePay",
                            1,
                            true,
                            null
                        );
                    }

                    $orderExists->update(
                        [
                            "payment_status" => StatusDefineCode::PAID,
                            'remaining_amount' =>  0,
                            'cod' =>  0,
                        ]
                    );
                }

                $link_back = null;
                $linkBackExists = LinkBackPay::where('order_id',  $orderExists->id)->first();
                if (!empty($linkBackExists->link_back)) {
                    $link_back = $linkBackExists->link_back;
                }


                return response()->view(
                    'success_paid',
                    [
                        'link_back' =>  $link_back
                    ]
                );
            }
        } else {
            $orderExists = Order::where(
                'order_code',
                $inputData['vpc_OrderInfo']
            )->first();

            $link_back = null;
            $linkBackExists = LinkBackPay::where('order_id',  $orderExists->id)->first();
            if (!empty($linkBackExists->link_back)) {
                $link_back = $linkBackExists->link_back;
            }
            return response()->view(
                'error_pay',
                [
                    'link_back' =>  $link_back
                ]
            );
        }
    }

    public function ipn(Request $request)
    {
        $inputData = $_GET;
        $dataPay = $this->getDataPay($request);
        $SECURE_SECRET = $dataPay['hascode'] ?? "";
        if (!$this->verifySecureHash($inputData, $SECURE_SECRET)) {
            return response()->json(['success' => false, 'message' => 'Secure hash verification failed.'], 400);
        }
        unset($_GET["vpc_SecureHash"]);
        $order = Order::where(
            'order_code',
            $inputData['vpc_OrderInfo']
        )->first();

        $orderId = $inputData['vpc_MerchTxnRef'] ?? null;

        $transactionStatus = $inputData['vpc_TxnResponseCode'] ?? null;
        if (strlen($SECURE_SECRET) > 0 && $orderId && $transactionStatus === '0') {
            if ($order != NULL) {
                if ($order->payment_status == 0) {
                    PushNotificationUserJob::dispatch(
                        $request->store->id,
                        $request->store->user_id,
                        'Shop ' . $order->store->name,
                        'Đơn hàng ' . $order->order_code . ' đã được thanh toán',
                        TypeFCM::CUSTOMER_PAID,
                        $order->order_code,
                        null
                    );

                    if ($order->customer_id != null) {
                        StatusDefineCode::saveOrderStatus(
                            $request->store->id,
                            $order->customer_id,
                            $order->id,
                            "Đã thanh toán đơn hàng qua Onepay",
                            1,
                            true,
                            null
                        );
                    }

                    $order->update(
                        [
                            "payment_status" => StatusDefineCode::PAID,
                            'remaining_amount' =>  0,
                            'cod' =>  0,
                        ]
                    );

                    $responseString = 'responsecode=1&desc=confirm-success';
                    return response($responseString, 200)
                        ->header('Content-Type', 'text/plain');
                }
            } else {
                $responseString = 'responsecode=0&desc=confirm-fail';
                return response($responseString, 200)
                    ->header('Content-Type', 'text/plain');
            }
        } else {
            $responseString = 'responsecode=0&desc=confirm-fail';
            return response($responseString, 200)
                ->header('Content-Type', 'text/plain');
        }
    }

    public function return(Request $request)
    {

        $dataPay = $this->getDataPay($request);
        $SECURE_SECRET = $dataPay['hascode'] ?? "";

        $vpc_Txn_Secure_Hash = $_GET["vpc_SecureHash"];
        unset($_GET["vpc_SecureHash"]);
        $errorExists = false;
        ksort($_GET);

        $inputData = $_GET;

        if (strlen($SECURE_SECRET) > 0 && $_GET["vpc_TxnResponseCode"] != "7" && $_GET["vpc_TxnResponseCode"] != "No Value Returned") {

            //$stringHashData = $SECURE_SECRET;
            //*****************************khởi tạo chuỗi mã hóa rỗng*****************************
            $stringHashData = "";

            // sort all the incoming vpc response fields and leave out any with no value
            foreach ($_GET as $key => $value) {
                //        if ($key != "vpc_SecureHash" or strlen($value) > 0) {
                //            $stringHashData .= $value;
                //        }
                //      *****************************chỉ lấy các tham số bắt đầu bằng "vpc_" hoặc "user_" và khác trống và không phải chuỗi hash code trả về*****************************
                if ($key != "vpc_SecureHash" && (strlen($value) > 0) && ((substr($key, 0, 4) == "vpc_") || (substr($key, 0, 5) == "user_"))) {
                    $stringHashData .= $key . "=" . $value . "&";
                }
            }
            //  *****************************Xóa dấu & thừa cuối chuỗi dữ liệu*****************************
            $stringHashData = rtrim($stringHashData, "&");


            //    if (strtoupper ( $vpc_Txn_Secure_Hash ) == strtoupper ( md5 ( $stringHashData ) )) {
            //    *****************************Thay hàm tạo chuỗi mã hóa*****************************
            if (strtoupper($vpc_Txn_Secure_Hash) == strtoupper(hash_hmac('SHA256', $stringHashData, pack('H*', $SECURE_SECRET)))) {
                // Secure Hash validation succeeded, add a data field to be displayed
                // later.
                $hashValidated = "CORRECT";
            } else {
                // Secure Hash validation failed, add a data field to be displayed
                // later.
                $hashValidated = "INVALID HASH";
            }
        } else {
            // Secure Hash was not validated, add a data field to be displayed later.
            $hashValidated = "INVALID HASH";
        }


        if ($hashValidated  == "CORRECT" && $_GET["vpc_TxnResponseCode"] == "0") {

            $historyExists = StatusPaymentHistory::where(
                'order_code',
                $inputData['vpc_OrderInfo']
            )->orderBy('id', 'desc')->first();

            $orderExists = Order::where(
                'order_code',
                $inputData['vpc_OrderInfo']
            )->first();

            if (empty($historyExists)) {
                StatusPaymentHistory::create(
                    [
                        "order_code" => $inputData['vpc_OrderInfo'],
                        "transaction_no" => $inputData['vpc_TransactionNo'],
                        "amount" => ($inputData['vpc_Amount'] != null && $inputData['vpc_Amount'] > 0) ? $inputData['vpc_Amount'] / 100 : 0,
                        "bank_code" =>  "",
                        "card_type" =>  $inputData['vpc_Card'] ?? "",
                        "order_info" => $inputData['vpc_OrderInfo'],
                        "pay_date" => Helper::getTimeNowString(),
                        "response_code" => $inputData['vpc_TxnResponseCode'] ?? "",
                        "key_code_customer" => "",
                    ]
                );

                if (!empty($orderExists)) {

                    PushNotificationUserJob::dispatch(
                        $request->store->id,
                        $request->store->user_id,
                        'Shop ' . $orderExists->store->name,
                        'Đơn hàng ' . $orderExists->order_code . ' đã được thanh toán',
                        TypeFCM::CUSTOMER_PAID,
                        $orderExists->order_code,
                        null
                    );

                    if ($orderExists->customer_id != null) {
                        StatusDefineCode::saveOrderStatus(
                            $request->store->id,
                            $orderExists->customer_id,
                            $orderExists->id,
                            "Đã thanh toán đơn hàng qua OnePay",
                            1,
                            true,
                            null
                        );
                    }

                    $orderExists->update(
                        [
                            "payment_status" => StatusDefineCode::PAID,
                            'remaining_amount' =>  0,
                            'cod' =>  0,
                        ]
                    );
                }
            }


            if ($_GET['vpc_TxnResponseCode'] == '0') {


                if (!empty($orderExists)) {
                    $orderExists->update(
                        [
                            "payment_status" => 2,
                        ]
                    );
                }

                $link_back = null;
                $linkBackExists = LinkBackPay::where('order_id',  $orderExists->id)->first();
                if (!empty($linkBackExists->link_back)) {
                    $link_back = $linkBackExists->link_back;
                }


                return response()->view(
                    'success_paid',
                    [
                        'link_back' =>  $link_back
                    ]
                );
            } else {


                return response()->view(
                    'error_pay',
                    [
                        'link_back' =>  null
                    ]
                );
                echo "Thanh toán không thành công xin thử lại";
            }
        } else {
            return response()->view(
                'error_pay',
                [
                    'link_back' =>  null
                ]
            );
            echo "Giao dịch không thành công";
        }
    }
}
