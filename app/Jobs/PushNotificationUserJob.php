<?php

namespace App\Jobs;

use App\Helper\Helper;
use App\Models\ConfigNotification;
use App\Models\CustomerDeviceToken;
use App\Models\NotificationCustomer;
use App\Models\NotificationUser;
use App\Models\UserDeviceToken;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use GuzzleHttp\Client;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class PushNotificationUserJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $store_id;
    protected $user_id;
    protected $content;
    protected $title;
    protected $type;
    protected $references_value;
    protected $branch_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        $store_id,
        $user_id,
        $title,
        $content,
        $type,
        $references_value,
        $branch_id
    ) {


        $this->store_id = $store_id;
        $this->user_id = $user_id;
        $this->title = $title;
        $this->content = $content;
        $this->type = $type;
        $this->references_value = $references_value;
        $this->branch_id = $branch_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notification_unread = 0;

        $notification_unread = NotificationUser::where('store_id', $this->store_id  ?? null)
            ->where(function ($query) {
                $query->where(
                    'branch_id',
                    $this->branch_id
                )->orWhere('branch_id', "=", null);
            })
            ->where('unread', true)->count();

        $notification_unread = $notification_unread + 1;


        $deviceTokens = UserDeviceToken::where('user_id',  $this->user_id)
            ->pluck('device_token')
            ->toArray();

        $deviceTokens = array_unique($deviceTokens);

        $data = [
            'body' => $this->content,
            'title' =>  $this->title,
            'type' => $this->type,
            'references_value' => $this->references_value,
            'badge' => (int)$notification_unread
        ];

        $key = Helper::getRandomOrderString();

        $this->sendNotification($data, $key, $deviceTokens);

        NotificationUser::create([
            'store_id' => $this->store_id  ?? null,
            "content" => $this->content,
            "title" => $this->title,
            "type" =>  $this->type,
            "unread" => true,
            'references_value' => $this->references_value,
            'branch_id' =>  $this->branch_id,
        ]);
    }

    public function sendNotification($data, $topicName = null, $tokens)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $data1 = [
            'title' => $data['title'] ?? 'Something',
            'body' => $data['body'] ?? 'Something',
            'icon' => 'myicon',
            'click_action' => 'https://example.com',
            "to" => "my_device_token",
            "collapse_key" => "type_a",
            "priority" => "high",
            "sound" => "default",
            'notification' => [
                'body' => $data['body'] ?? 'Something',
                'title' => $data['title'] ?? 'Something',
                'references_value' => $data['references_value'] ?? null,
                'sound' => 'default',
            ],
            "webpush" =>  [
                "headers" => [
                    "Urgency" => "high"
                ]
            ],
            "android" =>  [
                "priority" => "high"
            ],
            "priority" =>  'high',
            "sound" => "alarm",
            'data' => (object) [
                'branch_id' => $data['branch_id'] ?? null,
                'references_value' => $data['references_value'] ?? null,
                'title' => $data['title'] ?? null,
                'type' => $data['type'] ?? null,
                'url' => $data['url'] ?? null,
                'redirect_to' => $data['redirect_to'] ?? null,
                'type' => $data['type'] ?? null,
                "sound" => "alarm",
                "click_action" => "FLUTTER_NOTIFICATION_CLICK",
                'badge' => $data['badge'] ? (int)$data['badge'] : null,
            ],
            'apns' => [
                "headers" => [
                    "apns-collapse-id" => "solo_changed_administrator",
                    "content-available" => "1",
                    "apns-priority" => "10",
                ],
                'payload' => [
                    'aps' => [
                        // 'mutable-content' => 1,
                        // 'sound' => 'saha',
                        // "content-available" => "1",
                        // 'badge' => $data['badge'] ? (int)$data['badge'] : "0",
                        'sound' => 'slow_spring_board.aiff',
                    ],
                ],
                'fcm_options' => [
                    'image' => $data['image'] ?? null,
                ],
            ],
        ];

        $this->execute($url, $data1, "POST", $tokens, $data);
    }

    /**
     * @param $url
     * @param array $dataPost
     * @param string $method
     * @return bool
     * @throws GuzzleException
     */
    private function execute($url, $dataPost = [], $method = 'POST', $tokens, $data)
    {
        $result = false;
        try {
            // $client = new Client();
            $client = new Client([
                'headers' => [
                    'Authorization' => 'key=' . "AAAAZp-ZdHc:APA91bHwWGdX8i3rMW6D7QEQoVnQkhEqKbt2P7nj_bT5v3MV6y2aDjH-ozEUBm7nDMck1i9_1NwzVkIJ0GICAVWKCwStKVmjBSDbuQCrF3tK7OyTnpa49D9qRXHIkHbWE27IuxWD0dAb",
                    'Content-Type' => 'application/json',
                ],
            ]);

            $result = $client->post('https://fcm.googleapis.com/fcm/send', [
                'json' => [
                    'registration_ids' => $tokens,
                    'notification' => $dataPost,
                    'data' => $data
                ],
            ]);

            $result = $result->getStatusCode() == Response::HTTP_OK;
        } catch (Exception $e) {
            Log::debug($e);
        }

        return $result;
    }
}
