<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Base\BaseModel;
class AgencyImportConfig extends BaseModel
{
    use HasFactory;
    protected $guarded = [];
    
    protected $casts = [
        'is_end' => 'boolean',
    ];
}
